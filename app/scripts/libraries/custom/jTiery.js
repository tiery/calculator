(function(doc) {
	
	var $ = {};
	
// Simple DOM query
	$.query = function (selector) {
		return doc.querySelectorAll(selector);
	}
	
// Events binding
	function addEvent (el, eventType, handler) {
		if (el.addEventListener) { // DOM Level 2 browsers
			el.addEventListener(eventType, handler, false);
		} else if (el.attachEvent) { // IE <= 8
			el.attachEvent('on' + eventType, handler);
		} else { // ancient browsers
			el['on' + eventType] = handler;
		}
	}
	
	$.on = function (element, eventType, callback) {
		addEvent (element, eventType, callback);
	}
	$.off = function (element, eventType) {
		addEvent (element, eventType, null);
	}
	
// Loop
	$.each = function (elements, callback) {
		var i = 0,
			l = elements.length,
			element;
		for (; i < l; i++) {
			element = elements[i];
			callback.apply(element, [i]);
		}
	}

// In array
	$.inArray = function (find, arr) {
		var i = 0,
			l = arr.length,
			findIndex = -1;
		for (; i<l; i++) {
			if (arr[i] === find) {
				findIndex = i;
				break;
			}
		}
		return findIndex;
	}
	
// Expose
	window.jTiery = $;
	
}(document));