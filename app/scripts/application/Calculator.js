// Application wrapper
(function (appName, $) {
	
	'use strict';
	
	// Constructor
	function Calculator (element) {
		
		// Dom elements
		this.dom_main = element;
		this.dom_screen = element.querySelectorAll('[data-screen]')[0];
		this.dom_keys = $.query('[data-key]');
		
		// Create computer
		this.computer = new Computer(this.dom_screen);
		
		// Here we go !
		this.init();
	}
	
	// Initialization
	Calculator.prototype.init = function () {
		this.bindAppInterface();
		//this.bindKeyboard();
	}
	
	// Bindings
	Calculator.prototype.bindAppInterface = function () {
		var that = this;
		$.each(this.dom_keys, function bindKeys (index) {
			$.on(this, 'click', function(){
				var keyValue = this.getAttribute('data-key');
				that.computer.analyser(keyValue);
			});
		});
	}
	
	// Expose constructor
	window.Calculator = Calculator;
	
}('calculator', jTiery));