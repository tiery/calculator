// Application wrapper
(function(appName) {
	
	// Constructor
	function Screen (element) {
		this.status;
		this.dom_main = element;
		this.dom_output = element.querySelectorAll('[data-calculator="output"]')[0];
		this.init();
	}
	
	Screen.prototype.init = function () {
		this.print('');
		this.setStatusWait();
	}
	
	// API
	Screen.prototype.print = function (value) {
		if (value === '') {
			this.setStatusWait();
		}
		else {
			this.setStatusBusy();
		}
		this.dom_output.textContent = value;
	}
	
	Screen.prototype.getStatus = function () {
		return this.status;
	}
	Screen.prototype.setStatus = function (newStatus) {
		var currentStatus = this.getStatus();
		if (currentStatus !== newStatus) {
			this.dom_main.setAttribute('data-status', newStatus);
			this.status = newStatus;
		}
	}
	Screen.prototype.setStatusBusy = function () {
		this.setStatus('busy');
	}
	Screen.prototype.setStatusWait = function () {
		this.setStatus('wait');
	};
	
	// Expose
	window.Screen = Screen;
	
}('screen'));