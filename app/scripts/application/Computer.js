// Application wrapper
(function(appName, $) {
	
	// Helper
	function isCommand (value) {
		var bool = false;
		if ($.inArray(value, ['+', '-', '/', '*', '=', 'negation', 'undo', 'reset']) !== -1) {
			bool = true;
		}
		return bool;
	}
	
	// Constructor
	function Computer (screenElement) {
		this.sequence = '';
		this.executed = false;
		this.screen = new Screen(screenElement);
	}
	
	// API
	Computer.prototype.analyser = function (value) {
		if (value === 'reset') {
			this.reset();
		}
		else if (value === 'undo') {
			this.undo();
		}
		else if (value === '=') {
			this.execute();
		}
		else {
			if (this.executed && !isCommand(value)) {
				this.sequence = '';
			}
			this.executed = false;
			this.process(value);
		}
	}
	
	Computer.prototype.process = function (value) {
		var sequence = this.sequence;
		if (this.valid(value)) {
			// Si on tappe le premier caractère
			if (sequence === '' && value === '.') {
				value = '0' + value;
			}
			this.sequence += value.toString();
			this.screen.print(this.sequence);
		}
	}
	
	Computer.prototype.valid = function (value) {
		var sequence = this.sequence,
			bool = true;
		
		// Pas 2 zérose consécutifs en début de séquence
		if (value === '0' && sequence.indexOf('0') === 0 && sequence.length === 1) {
			bool = false;
		}
		
		// Pas plus d'une virgule dans la séquence
		if (value === '.' && sequence.indexOf('.') > -1) {
			bool = false;
		}
		
		if (sequence === '' && isCommand(value)) {
			bool = false;
		}
		
		return bool;
	}
	
	Computer.prototype.execute = function () {
		var equation = this.sequence,
			result;
			
		if (equation === '') {
			return;
		}
		
		try {
			result = +eval(equation).toFixed(2);
			this.executed = true;
			this.sequence = result;
			this.screen.print(result);
		}
		catch (e) {
			this.executed = false;
			this.sequence = '';
			this.screen.print('Error');
		}
		
		
	}
	
	Computer.prototype.reset = function () {
		this.sequence = '';
		this.screen.print('');
	}
	
	Computer.prototype.undo = function () {
		if (this.screen.getStatus() === 'wait') {
			return;
		}
		var currentSequence = this.sequence;
		this.sequence = currentSequence.substr(0, currentSequence.length - 1);
		this.screen.print(this.sequence);
	}
	
	// Expose
	window.Computer = Computer;
	
}('computer', jTiery));